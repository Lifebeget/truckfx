import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    withStyles,
    Grid,
    TextField,
    Button
} from "@material-ui/core";
import { Formik } from 'formik/dist/index';
import validation_schema from './validation_schema';
import gql from "graphql-tag";
import { Mutation} from "react-apollo";
import { Redirect } from "react-router-dom";
import LogIn from './../sign-in/LogIn';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '2.5em auto 0'
    },
    itemGrid: {
        textAlign: 'center'
    },
    textField: {
        padding: theme.padding / 2,
        minWidth: '350px',
        width: '35%',
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    button: {
        margin: theme.spacing.unit,
        maxWidth: '200px'
    },
   errorStyle: {
        color: 'red',
       textDecoration: 'underline'
   }
});


const EDIT_EMPLOYEE = gql`
    mutation EditEmployee($id: ID!, $input: EmployeeInput!) {
        editEmployee(id: $id, input: $input) {
            id
            status
            firstName
            lastName
            middleName
            address
            city
            state
            zip
            userName
            phoneNumber
            officeName
            email
            extension
            paid
            ssn
            alert
        }
    }

`;

// CONTINUE HERE
class CreatePassword extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoginComponent: false
        };
    }

    componentDidMount() {
        if ( this.props.resetToken() ) {
            this.props.resetToken();
        }
    }

    changeIsLoginState = () => {
        this.setState(() => {
            return {
                isLoginComponent: !this.state.isLoginComponent
            }
        })
    };

    render() {

      const { classes } = this.props;

      let id_from_path = '';

      return (
          <div className="loginpage">
              {!this.state.isLoginComponent ? <Mutation
                      mutation={EDIT_EMPLOYEE}
                  >
                      {(editEmployee, {data, loading}) => (
                          <Formik
                              initialValues={{password: undefined}}
                              validationSchema={validation_schema}
                              onSubmit={(values, actions) => {
                                  console.log('');
                                  id_from_path = (window.location.pathname).slice(window.location.pathname
                                      .indexOf('d/') + 2);
                                  editEmployee({
                                      variables: {
                                          input: {password: values.password},
                                          id: id_from_path
                                      }
                                  }).then((res) => {
                                      alert(res.data.editEmployee.alert);
                                       // this.changeIsLoginState();
                                      localStorage.removeItem('token');
                                      window.location.href = '/';
                                  });
                              }}
                              render={props => (
                                  <form onSubmit={props.handleSubmit}>
                                      <Grid container className={classes.container} >
                                          <Grid className={classes.itemGrid} item xs={12}>
                                              <TextField
                                                  name={'password'}
                                                  onChange={props.handleChange}
                                                  id="outlined-password-input"
                                                  label="Password"
                                                  className={classes.textField}
                                                  type="password"
                                                  autoComplete="current-password"
                                                  margin="normal"
                                                  // variant="outlined2"
                                                  value={props.values.password}
                                                  error={props.errors.password ? true : false}
                                              />
                                              {props.errors.password &&
                                              <div className={classes.errorStyle}>{props.errors.password}</div>}
                                          </Grid>
                                          <Grid className={classes.itemGrid} item xs={12}>
                                              <TextField
                                                  name={'confirm_password'}
                                                  onChange={props.handleChange}
                                                  id="outlined-password-input2"
                                                  label="Confirm Password"
                                                  className={classes.textField}
                                                  type="password"
                                                  autoComplete="current-password"
                                                  margin="normal"
                                                  // variant="outlined"
                                                  value={props.values.confirm_password}
                                                  error={props.errors.confirm_password ? true : false}
                                              />
                                              {props.errors.confirm_password &&
                                              <div className={classes.errorStyle}>{props.errors.confirm_password}</div>}
                                          </Grid>
                                      </Grid>

                                      <Grid className={classes.itemGrid} item xs={12}>
                                          <Button type={'submit'} variant="contained" size="medium" color="primary"
                                                  className={classes.button}>
                                              Create Password
                                          </Button>
                                      </Grid>
                                  </form>
                              )
                              }
                          />
                      )}
                  </Mutation> :
                  <Redirect to={`/login`}>
                      <LogIn/>
                  </Redirect>
              }
          </div>
      )
  }
}

CreatePassword.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreatePassword);

