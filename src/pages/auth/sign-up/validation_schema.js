import * as Yup from 'yup'

export default Yup.object().shape({
    password: Yup.string().required("Enter the password"),
    confirm_password: Yup.string().required('Confirm the password')
        .oneOf([Yup.ref("password")], "Passwords do not match")
});