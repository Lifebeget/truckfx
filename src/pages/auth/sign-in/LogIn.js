import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  withStyles,
  Grid,
  TextField,
  Button
} from "@material-ui/core";
import { Formik } from 'formik';
import validation_schema from './validation_schema';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Redirect } from "react-router-dom";


const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    margin: '2.5em auto 0'
  },
  itemGrid: {
    textAlign: 'center'
  },
  textField: {
    padding: theme.padding / 2,
    minWidth: '350px',
    width: '35%',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  button: {
    margin: theme.spacing.unit,
    maxWidth: '200px'
  },
  errorStyle: {
    color: 'red',
    textDecoration: 'underline'
  }
});


const SIGN_IN_EMPLOYEE = gql`
  mutation SignInEmployees($input: EmployeeInput!) {
    sign_in_employee(input: $input) {
      email
      password
      token
      alert
      unique_identifier
      is_admin_login
      is_forgot_password
    }
  }
`;

class LogIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAdminLogin: false,
      isMasterKeyFocused: false,
      isPasswordFocused: false,
      isForgotPassword: false
    };

    this.initial_values = {
      email: '',
      password: '',
      master_key: '',
      unique_identifier: '',
      is_admin_login: false,
      is_forgot_password: this.state.isForgotPassword
    }
  }

  changeCheckBoxState = e => {
    let target = e.target;
    let name = target.name;
    let checkedValue = target.checked;

  this.setState({[name]: checkedValue, isAdminLogin: name === 'isForgotPassword' ? false :
        (this.state.isAdminLogin) ? false : true, isMasterKeyFocused: false, isPasswordFocused: false}, () => {
    this.initial_values.is_forgot_password = this.state.isForgotPassword
  });
  };

  onFocus = e => {
    let for_focused = '';
    if (e.target.name === 'master_key') {
      for_focused = 'isMasterKeyFocused';
    }
    else {
      for_focused = 'isPasswordFocused';
    }
    this.setState(() => {
      return {
        [for_focused]: true
      }
    })
  };

  onBlur = e => {
    let for_onblured = '';
    if (e.target.name === 'master_key') {
      for_onblured = 'isMasterKeyFocused';
    }
    else {
      for_onblured = 'isPasswordFocused';
    }
    this.setState(() => {
      return {
        [for_onblured]: false
      }
    })
  };


  render() {
    const { classes } = this.props;
    return (
      <div className="login">
        {this.props.isAuth &&
            <Mutation
                mutation={SIGN_IN_EMPLOYEE}
            >
              {(sign_in_employee, { data, loading }) => (
                  <Formik
                      initialValues={this.initial_values}
                      validationSchema={validation_schema}
                      onSubmit={(values, actions) => {
                       sign_in_employee({
                          variables: {
                            input: {
                              email: values.email,
                              password: values.password,
                              master_key: values.master_key,
                              unique_identifier: localStorage.getItem('unique_identifier'),
                              is_admin_login: this.state.isAdminLogin,
                              is_forgot_password: this.initial_values.is_forgot_password
                            },
                          }
                        }).then((res) => {
                          let response = res.data.sign_in_employee;
                          this.setState({isAdminLogin: response.is_admin_login});

                          if (response.unique_identifier) {
                            this.setState({isAdminLogin: false}, () => {
                              localStorage.setItem('unique_identifier', response.unique_identifier);
                            });
                          }
                          if (response.token) {
                            localStorage.setItem('token', response.token);
                            this.props.change_is_auth_state();
                            window.location.reload();
                          }
                          alert(response.alert);
                        }).catch(err => alert(err.stack));
                      }}
                      render={props => {
                        return (
                          <form onSubmit={props.handleSubmit}>
                            <Grid container className={classes.container}>
                              <Grid className={classes.itemGrid} item xs={12}>
                                <TextField
                                    name={'email'}
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    id="outlined-email-input"
                                    label="Email"
                                    className={classes.textField}
                                    type="text"
                                    autoComplete="current-email"
                                    margin="normal"
                                    variant="outlined"
                                    value={props.values.email}
                                     error={props.errors.email ? true : false}
                                />
                                {props.errors.email && props &&
                                <div className={classes.errorStyle}>Enter Email</div>}
                              </Grid>
                              {!this.state.isForgotPassword && <Grid className={classes.itemGrid} item xs={12}>
                                <TextField
                                    name={'password'}
                                    onChange={props.handleChange}
                                    onFocus={this.onFocus}
                                    onBlur={this.onBlur}
                                    id="outlined-login-passsword-input"
                                    label="Login Password"
                                    className={classes.textField}
                                    type="password"
                                    autoComplete="current-login-password"
                                    margin="normal"
                                    variant="outlined"
                                    value={props.values.password}
                                    error={(!props.values.password && this.state.isPasswordFocused) ? true : false}
                                />
                                {!props.values.password && this.state.isPasswordFocused &&
                                <div className={classes.errorStyle}>{props.errors.password}</div>}
                              </Grid>}
                              {!this.state.isAdminLogin && <Grid className={classes.itemGrid} item xs={9}>
                                <FormControlLabel
                                    control={
                                      <Checkbox
                                          name={'isForgotPassword'}
                                          checked={this.state.isForgotPassword}
                                          onChange={this.changeCheckBoxState}
                                          value="checkedB"
                                          color="primary"
                                      />
                                    }
                                    label="Forgot Password"
                                />
                              </Grid>}
                              {this.state.isAdminLogin && !this.state.isForgotPassword && <Grid className={classes.itemGrid} item xs={12}>
                                <TextField
                                    name={'master_key'}
                                    onChange={props.handleChange}
                                    onFocus={this.onFocus}
                                    onBlur={this.onBlur}
                                    id="outlined-master_key-input"
                                    label="Master_Key"
                                    className={classes.textField}
                                    type="password"
                                    autoComplete="current-master_key"
                                    margin="normal"
                                    variant="outlined"
                                    value={props.values.master_key}
                                    error={!props.values.master_key && this.state.isMasterKeyFocused ? true : false}
                                />
                                {!props.values.master_key && this.state.isMasterKeyFocused &&
                                <div className={classes.errorStyle}>Enter Master Key</div>}
                              </Grid>}
                            </Grid>

                            <Grid className={classes.itemGrid} item xs={12}>
                              {<Button type={'submit'} variant="contained" size="medium" color="primary"
                                      className={classes.button}>
                                {this.state.isForgotPassword && 'CHANGE PASSWORD'}
                                {!this.state.isAdminLogin && !this.state.isForgotPassword && 'LOGIN'}
                                {this.state.isAdminLogin && !this.state.isForgotPassword && 'ADMIN LOGIN'}
                              </Button>}

                              {!this.state.isForgotPassword &&<FormControlLabel
                                  control={
                                    <Checkbox
                                        name={'isAdminLogin'}
                                        checked={this.state.isAdminLogin}
                                        onChange={this.changeCheckBoxState}
                                        value="checkedB"
                                        color="primary"
                                    />
                                  }
                                  label="Switch Admin Login"
                              />}
                            </Grid>
                          </form>
                      )}
                      }
                  />
              )}
            </Mutation>}
        {!this.props.isAuth && <Redirect to={'/'} />}
      </div>
    )
  }
}

LogIn.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LogIn);
