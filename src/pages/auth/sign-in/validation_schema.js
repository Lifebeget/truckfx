import * as Yup from 'yup'

export default Yup.object().shape({
    email: Yup.string().email('Enter correct email.').required('Enter email.')
});
