import ApolloClient from "apollo-client";
import { InMemoryCache } from 'apollo-cache-inmemory';
import {getAuthToken} from './shared/functionalServiceModule';
import { createUploadLink } from 'apollo-upload-client';

import { split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'

const token = getAuthToken().token;

const linkOptions = {
    uri: 'http://localhost:4000',
    headers: {
        authorization: token ? `Bearer ${token}` : '',
        role: token ? getAuthToken().decoded_jwt.role : ''
    }
};

const httpLink = createUploadLink(linkOptions);

const wsLink = new WebSocketLink({
    uri: `ws://localhost:4000`,
    options: {
        reconnect: true
    }
});

const link = split(
    ({ query }) => {
        const { kind, operation } = getMainDefinition(query);
        return kind === 'OperationDefinition' && operation === 'subscription'
    },
    wsLink,
    httpLink
);


const client =  new ApolloClient({
    link,
  cache: new InMemoryCache({
    addTypename: false,
    fragmentMatcher: true
  }),

  onError: ({ graphQLErrors, networkError}) => {
    if (graphQLErrors) {
      console.log('Graphql Error');
      window.location.href = '/login';
    }
    if (networkError) {
      console.log('Network Error');
    }
  },
});


export default client;
